# BBC Code Test

Code for BBC Coding Kata - Roman Numerals

#Installation

In the root folder (where package.json file is)
Command Line Run:- npm Install

To run via the index file - will just output some rudimentary tests
RUN:- npm run start

To run test suites - Test suites running on present solution and former solutions
mmmim_index.js && mmmcmxcix_index.js - contained as backups

RUN:- npm run test


#Solution
I have kept reference to the first solution in backups
This felt very convoluted and unwieldy 

Subsequent to that I used a basic switch case because of it's simplicity
and it seems to match the requirements of the conversion quite nicely
making it all together more readable and straight forward

#MMMIM
For some reason I got it my head that this return was the standard required for 3999 MMMIM rather than MMMCMXCIX
See my script solution for this ./src/backup/mmmim_index.js
test file ./tests/mmmim_roman_numerals.test.js

#MMMCMXCIX
The actual required solution 
See my script solution for this ./src/backup/mmmcmxcix_index.js
test file ./tests/class_roman_numerals.test.js

#Completion
The actual requirements specified a class based solution 
./src/lib/integer_roman_numeral_converter.js
class really is little more than just a structured implementation that fulfills the interface requirements
test file ./tests/class_roman_numerals.test.js



#Tests
Simple implementation of testing validity and range of inputs
and the acuracy of received vs expected results

tests for each implementation - except the first are included in the execution
These tests are the same with one exception
The expected results for 3999 are different between 
mmmcmxcix_index.js, integer_roman_numeral_converter.js
and 
mmmim_index.js
with 
MMMCMXCIX 
&&
MMMIM
respectively
